<?php
declare(strict_types=1);

use CryptoCurrencyChart\API\Client;

require '../src/autoload.php';

/** @var string $symbol The symbol of the coin to create a chart for */
$symbol = 'ETH';
/** @var Client $client The API client, set your API key and secret here to run this example */
$client = new Client('apiKey', 'apiSecret');

$coins = $client->getCoins();
foreach ($coins as $coin) {
	if ($coin->symbol !== $symbol) {
		continue;
	}

	$coinHistory = $client->viewCoinHistory($coin->id, new DateTime('-14 days'), new DateTime('now'));
	$high = 0;
	$low = PHP_FLOAT_MAX;
	foreach ($coinHistory->data as $priceData) {
		if ($priceData->value > $high) {
			$high = $priceData->value;
		}
		if ($priceData->value < $low) {
			$low = $priceData->value;
		}
	}

	$step = ($high - $low) / 3.0;
	vprintf("*** %s chart ***\n", [$coin->name]);
	foreach ($coinHistory->data as $priceData) {
		if ($priceData->value < $low + $step) {
			print('_');
		} elseif ($priceData->value < $high - $step) {
			print('~');
		} else {
			print('^');
		}
	}
	print("\n");
	vprintf("Low: $%s\nHigh: $%s\n", [number_format($low, 2), number_format($high, 2)]);
}

