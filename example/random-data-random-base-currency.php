<?php
declare(strict_types=1);

use CryptoCurrencyChart\API\Client;

require '../src/autoload.php';

/** @var Client $client The API client, set your API key and secret here to run this example */
$client = new Client('apiKey', 'apiSecret');

$coins = $client->getCoins();
foreach ($coins as $coin) {
	if ($coin->symbol !== 'XRP') {
		continue;
	}

	$dataTypes = $client->getDataTypes();
	$randomDataType = $dataTypes[array_rand($dataTypes)];
	$baseCurrencies = $client->getBaseCurrencies();
	$randomBaseCurrency = $baseCurrencies[array_rand($dataTypes)];

	$data = $client->viewCoinHistory(
		$coin->id,
		new DateTime('-10 days'),
		new DateTime(),
		$randomDataType,
		$randomBaseCurrency
	);

	// Note: The representation does not make sense for all data types, you can probably do a lot better
	vprintf("XRP %s:\n", [$randomDataType]);
	foreach ($data->data as $historyData) {
		vprintf("[%s] %s %s\n", [$historyData->date, $randomBaseCurrency, $historyData->value]);
	}
	break;
}

