<?php
declare(strict_types=1);

use CryptoCurrencyChart\API\Client;

require '../src/autoload.php';

/** @var Client $client The API client, set your API key and secret here to run this example */
$client = new Client('apiKey', 'apiSecret');

// Note: You would probably want to cache the coin list for at least a day, normally there is no need to keep fetching it each time
$coins = $client->getCoins();
foreach ($coins as $coin) {
	if ($coin->symbol !== 'BTC') {
		continue;
	}
	$btcData = $client->viewCoin($coin->id);
	vprintf("BTC: $%s\n", [number_format((float) $btcData->closePrice, 2)]);
	break;
}

