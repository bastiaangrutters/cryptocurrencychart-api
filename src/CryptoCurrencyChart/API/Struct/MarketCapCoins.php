<?php
declare(strict_types=1);

namespace CryptoCurrencyChart\API\Struct;


class MarketCapCoins extends Struct {
	/** @var int The result page number. */
	public int $page;
	/** @var int The maximum number of pages for the amount per page. */
	public int $maximumPage;
	/** @var int The amount of coins per page returned. */
	public int $amount;
	/** @var \CryptoCurrencyChart\API\Struct\Coin[] The coins which the data belong to. */
	public array $coins;


	public function toArray(): array {
		$data = parent::toArray();

		$data['coins'] = [];
		foreach ($this->coins as $coin) {
			$data['coins'][] = $coin->toArray();
		}

		return $data;
	}

	public static function fromArray(array $data): Struct {
		if (isset($data['coins']) && is_array($data['coins'])) {
			$coins = [];
			foreach ($data['coins'] as $coin) {
				$coins[] = Coin::fromArray($coin);
			}
			$data['coins'] = $coins;
		}
		return parent::fromArray($data);
	}
}