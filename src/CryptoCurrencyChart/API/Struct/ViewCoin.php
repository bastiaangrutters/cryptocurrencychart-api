<?php
declare(strict_types=1);

namespace CryptoCurrencyChart\API\Struct;


class ViewCoin extends Struct {
	/** @var \CryptoCurrencyChart\API\Struct\Coin The coin */
	public Coin $coin;
}