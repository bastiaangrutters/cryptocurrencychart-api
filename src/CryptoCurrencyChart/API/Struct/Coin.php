<?php
declare(strict_types=1);

namespace CryptoCurrencyChart\API\Struct;


class Coin extends Struct {
	/** @var int The coin id */
	public int $id;
	/** @var string The name of the coin */
	public string $name;
	/** @var string The symbol for the coin. */
	public string $symbol;
	/** @var string|null The base currency used in the coins values. */
	public ?string $baseCurrency;
	/** @var string|null The date for the price data formatted as YYYY-mm-dd.  */
	public ?string $date;
	/** @var float|null The volume weighted average price for the coin on the provided date. */
	public ?float $price;
	/** @var float|null The open price for the coin on the provided date. */
	public ?float $openPrice;
	/** @var float|null The last price for the coin on the provided date, if the date is the current day it is the most recent price for the day. */
	public ?float $closePrice;
	/** @var float|null The highest price for the coin on the provided date. */
	public ?float $highPrice;
	/** @var float|null The lowest price for the coin on the provided date. */
	public ?float $lowPrice;
	/** @var float|null The market capitalization for the coin on the provided date. */
	public ?float $marketCap;
	/** @var float|null The trade volume for the coin on the provided date. */
	public ?float $tradeVolume;
	/** @var float|null The trade volume in markets against a fiat coin for the coin on the provided date. */
	public ?float $fiatTradeVolume;
	/** @var int|null The coin rank by market capitalization on the provided date. */
	public ?int $rank;
	/** @var float|null The number of coins available on the provided date. */
	public ?float $supply;
	/** @var float|null The percentage of the market capitalization of the coin that was traded on the provided date. */
	public ?float $tradeHealth;
	/** @var int|null The number of transactions, smart contract executions or other interactions with the coin. */
	public ?int $transactions;
	/** @var float|null The number of fees paid for the coin. */
	public ?float $fees;
	/** @var float|null The utility rating for the coin on the provided date. */
	public ?float $utilityRating;
	/** @var string|null A sentiment indication for the coins market on the provided date. */
	public ?string $sentiment;
	/** @var string|null The date of the first available data for the coin in the format of YYYY-mm-dd. */
	public ?string $firstData;
	/** @var string|null The date of the most recent data for the coin in the format of YYYY-mm-dd. */
	public ?string $mostRecentData;
	/** @var string|null The status of the coin. */
	public ?string $status;
}